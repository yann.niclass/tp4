//
// Created by Beat Wolf on 30.11.2021.
//

#ifndef CPPALGO_INTERVAL_H
#define CPPALGO_INTERVAL_H


template<typename T1 = int, typename T2 = double>
class Interval {
public:
    Interval(T1 start, T1 end, T2 value = 0) : start_(start), end_(end), value_(value) {}

    T1 getStart() const { return start_; }

    T1 getEnd() const { return end_; }

    T2 getValue() const { return value_; }

    //Overload Operators

    bool operator==(const Interval &other) const {
        return value_ == other.value_;
    }

    bool operator!=(const Interval &other) const {
        return !(this == other);
    }

    bool contains(const T2 value) const {
        return start_ <= value_ && value_ <= end_;
    }

    // float

    template<typename T>
    bool operator==(const Interval<float, T> &other) const {
        return abs(start_ - other.start_) < std::numeric_limits<float>::epsilon() &&
                abs(end_ - other.end_) < std::numeric_limits<float>::epsilon() &&
                value_ == other.value_;
    }

    template<typename T>
    bool operator==(const Interval<T, float> &other) const {
        return start_ == other.start_ &&
                end_ == other.end_ &&
                abs(value_ - other.value_) < std::numeric_limits<float>::epsilon();
    }


    // double

    template<typename T>
    bool operator==(const Interval<double, T> &other) const {
        return abs(start_ - other.getStart()) < std::numeric_limits<double>::epsilon() &&
                abs(end_ - other.getEnd()) < std::numeric_limits<double>::epsilon() &&
                value_ == other.getValue();
    }

    template<typename T>
    bool operator==(const Interval<T, double> &other) const {
        return start_ == other.start_ &&
                end_ == other.end_&&
                abs(value_ - other.value_) < std::numeric_limits<double>::epsilon();
    }

private:
    T1 start_;
    T1 end_;
    T2 value_;
};


#endif //CPPALGO_INTERVAL_H
