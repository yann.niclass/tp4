//
// Created by beatw on 4/29/2022.
//

#ifndef CPPALGO_INTERVALTREEELEMENT_H
#define CPPALGO_INTERVALTREEELEMENT_H

#include <vector>
#include <algorithm>
#include "Interval.h"

/**
 * Single element of the interval tree
 * @tparam T
 */
template<typename T>
class IntervalTreeElement {
public:
    /**
     * Building IntervalTreeElement
     * @param values
     * @param mid
     */
    IntervalTreeElement(std::vector<Interval<T>> &values, T mid) : mid(mid) {

        //fill attributes
        //TODO: Need to implement this part
    }

    /**
     * Return all Intervals that intersect with a given point
     * @param x
     * @return
     */
    std::vector<Interval<T>> intersecting(T x) {

        //TODO: Need to implement this part
    }

    std::vector<Interval<T>> leftSorted;
    std::vector<Interval<T>> rightSorted;
    T mid;
};

#endif //CPPALGO_INTERVALTREEELEMENT_H
